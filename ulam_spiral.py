#!/usr/bin/env python3

from graphics import *
import math

def draw_box(window, x, y, d, text, primarity=0):
    beg = Point(x - d/2, y - d/2)
    end = Point(x + d/2, y + d/2)
    r = Rectangle(beg, end)
    if primarity == True:
        if color_ver == 1:
            r.setFill(color_rgb(0, 0, 0))
            r.setOutline(color_rgb(255, 255, 255))
        elif color_ver == 2:
            r.setFill(color_rgb(34, 245, 122))
            r.setOutline(color_rgb(255, 255, 255))
        elif color_ver == 3:
            r.setFill(color_rgb(255, 0, 0))
            r.setOutline(color_rgb(0, 0, 0))
    else:
        if color_ver == 1:
            r.setFill(color_rgb(255, 255, 255))
            r.setOutline(color_rgb(255, 255, 255))
        elif color_ver == 2:
            r.setFill(color_rgb(244, 231, 122))
            r.setOutline(color_rgb(255, 255, 255))
        elif color_ver == 3:
            r.setFill(color_rgb(255, 255, 255))
            r.setOutline(color_rgb(0, 0, 0))
    r.draw(window)

    if show_numbers == True:
        t = Text(Point(x, y+1), text)
        t.setFace('courier')
        t.setStyle('bold')
        t.draw(window)


def is_prime(number):
    if number < 2:
        return False
    for i in range(2, int(math.sqrt(number)) + 1):
        if number % i == 0:
            return False
    return True


if __name__ == '__main__':
# <config>
# <window_size>
    width = 1600
    height = 1000
# </window_size>
# <dimensions>
    dim = 5
    distance = 0
# </dimensions>
# <other>
    to_draw = 1000000
    color_ver = 1
    show_numbers = False
# </other>
# </config>

    win = GraphWin("Ulam spiral", width, height)
    win.setBackground(color_rgb(255, 255, 255))

    begX = win.width / 2
    begY = win.height / 2
    direction = 1
    counter = 1
    no = 1

    while to_draw > 0:
        for j in range(counter):
            if to_draw == 0:
                break

            try:
                draw_box(win, begX, begY, dim, str(no), is_prime(no))
            except GraphicsError:
                to_draw = 0
                break

            to_draw -= 1
            no += 1

            if direction == 1:
                begX += (dim + distance)
            else:
                begX -= (dim + distance)

        for k in range(counter):
            if to_draw == 0:
                break

            try:
                draw_box(win, begX, begY, dim, str(no), is_prime(no))
            except GraphicsError:
                to_draw = 0
                break

            to_draw -= 1
            no += 1

            if direction == 1:
                begY -= (dim + distance)
            else:
                begY += (dim + distance)
        counter += 1
        direction = 0 if direction else 1

    try:
        win.getMouse()
    except GraphicsError:
        win.close()
    else:
        win.close()
