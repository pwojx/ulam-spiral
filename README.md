# ulam spiral

A simple program that draws an Ulam spiral of prime numbers.

![color\_ver: 3](screenshot1.png)<br />
![color\_ver: 2](screenshot2.png)<br />
![color\_ver: 1](screenshot3.png)
